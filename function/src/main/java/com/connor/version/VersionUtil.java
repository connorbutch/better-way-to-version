package com.connor.version;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class VersionUtil {
    private static final String PROPERTIES_FILE_NAME = "gitInfo.properties";
    private static final VersionUtil INSTANCE = new VersionUtil();

    private final Map<String, String> properties;
    private boolean hasLoaded = false;

    private VersionUtil(Map<String, String> properties) {
        this.properties = properties;
    }

    private VersionUtil() {
        this(new HashMap<>());
    }

    public static VersionUtil getInstance() {
        return INSTANCE;
    }

    public String getCommitHash() {
        if (needToLoadProperties()) {
            loadProperties();
        }
        return properties.get("hash");
    }

    private boolean needToLoadProperties() {
        return properties.isEmpty() && !hasLoaded;
    }

    private void loadProperties() {
        hasLoaded = true;
        synchronized (properties) {
            if (properties.isEmpty()) {
                try (InputStream is = getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME)) {
                    Properties prop = new Properties();
                    prop.load(is);
                    prop.forEach((key, value) -> properties.put((String) key, String.valueOf(value)));
                } catch (IOException e) {
                    String errorMessage = String.format("Error reading file at path %s", PROPERTIES_FILE_NAME);
                    throw new RuntimeException(errorMessage, e);
                }
            }
        }
    }
}
