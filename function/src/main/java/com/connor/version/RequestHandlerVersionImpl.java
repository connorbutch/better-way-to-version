package com.connor.version;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;

import java.util.HashMap;
import java.util.Map;

public class RequestHandlerVersionImpl implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent>  {
    private final VersionUtil versionUtil;

    public RequestHandlerVersionImpl(VersionUtil versionUtil) {
        this.versionUtil = versionUtil;
    }

    public RequestHandlerVersionImpl(){
        this(VersionUtil.getInstance());
    }

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent input, Context context) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        final String version = versionUtil.getCommitHash();
        System.out.println(version);
        //wouldn't really return to customers.... just for verification
        //TODO bring in x-ray and emf
        headers.put("x-version", version);
        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent()
                .withHeaders(headers);
            String output = "{ \"message\": \"hello world\" }";
            return response
                    .withStatusCode(200)
                    .withBody(output);

    }
}
